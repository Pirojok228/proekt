public class Student {
    private String FIO;
    private String NumGroup;
    private double[] yspevaemost;

    public Student(String FIO, String NumGroup, double[] yspevaemost){
        this.FIO = FIO;
        this.NumGroup = NumGroup;
        this.yspevaemost = yspevaemost;
    }
    public String getFIO(){
        return FIO;
    }
    public void  setFIO(String FIO){
        this.FIO = FIO;
    }
    public String getNumGroup(){
        return  getNumGroup();
    }
    public void setNumGroup(String NumGroup){
        this.NumGroup = NumGroup;
    }
    public double[] getYspevaemos(){
        return yspevaemost;
    }
    public void setYspevaemost(double[] yspevaemost){
        this.yspevaemost = yspevaemost;
    }
    public double getSrBall(){
        double sum = 0;
        int d = 0;
        for (int i= 0; i < 5; i++){
          sum += yspevaemost[i];
          d++;
        }
        return sum/d;
    }

@Override
    public String toString(){
        return String.format("ФИО: %s Группа: %s Успеваемость: %f, %f, %f, %f, %f Средний балл, %f",FIO,NumGroup,yspevaemost[0],yspevaemost[1],yspevaemost[2],yspevaemost[3],yspevaemost[4],getSrBall());
    }
}
